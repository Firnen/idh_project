  
###################################### Librairies ###################################### 

# BiocManager::install("STRINGdb") # installation si necessaire
# BiocManager::install("tidyverse") # installation si necessaire
library(STRINGdb)
library(tidyverse)



###################################### Donnees ###################################### 

# Table des especes 
tb_sp = get_STRING_species(version="10", species_name=NULL)

#### Changer le nom de l'id de l'espece ici #### 
id_sp = 214092 # par defaut : Yersinia Pestis C092

# Nom de l'espece recupere dans la table des especes tb_sp
name_sp = tb_sp[which(tb_sp[,1]==id_sp),2] 


# DOWNLOAD stringdb dans un dossier nomme repo_data  /!\ avoir cree le dossier repo_data auparavant  /!\
sdb = STRINGdb$new(version='10', species=id_sp, score_threshold=400, input_directory='repo_data')
# DOWNLOAD annotations
annots = sdb$get_annotations() 
# DOWNLOAD Prot et leurs annotations 
p = sdb$get_proteins()

# Filtre les annotations pour ne recuperer que les termes GO
annots_filt_go = filter(annots,(grepl("^GO:",annots[,2])==TRUE)) #

# Go termes de l'espece, sans redondance
list_go = unique(annots_filt_go$term_id) 

date = Sys.Date() # date



###################################### Neo4j connexion ###################################### 

# avoir configure neo4j pour l'organisme choisi
# lancer dans la console, dans le bin de neo4j : $ ./neo4j console
# lien : http://localhost:7474/browser/

library(neo4r)
connex = neo4j_api$new(url="http://localhost:7474", user="neo4j", password = "bioinfo")



###################################### Fichiers .sets : proteines directes, et directes et implicites ###################################### 

to_replace = paste(id_sp,'.',sep='') # pour ne garder que le nom des proteines sans le nom de l'espece devant


################# Proteines DIRECTES 
ouptut_file_dir_prots = paste(name_sp,'.directs.sets.txt',sep='')

write(paste("# format: sets \n# version: 1.0 \n# strain: ",name_sp,"\n# date : ",date,"\n# comment: Gene Ontology terms")
      , file = ouptut_file_dir_prots, append = FALSE, sep = "") # reecrit le fichier 


for (got in (1:length(list_go))) { 
  go_term = list_go[got]
  
  # recupere les goterm : acc, type, name
  query_term_direct = paste0("MATCH (g:GOTerm {acc : '", go_term, "'}) return g.acc, g.term_type, g.name ") # query neo4j
  go_term_direct = query_term_direct %>% call_neo4j(connex) # envoie la requete + sauvegarde le resultat
  
  if (length(go_term_direct$g.acc[[1]]) != 0) {   # verifie que la ligne du goterm recupere n'est pas vide
    
    # recupere les proteines correspondantes au goterm
    query_prot_direct = paste0("MATCH (g:GOTerm {acc : '", go_term, "'}) --> (p:Protein) return p.name") # query neo4j
    go_prot_direct = query_prot_direct %>% call_neo4j(connex) # envoie la requete + sauvegarde le resultat
    
    for (prot in (1:length(go_prot_direct$p.name[[1]]))){   # garde que le nom de la proteine, enleve le nom de l'espece devant
      go_prot_direct$p.name[[1]][prot] = str_replace(go_prot_direct$p.name[[1]][prot],(to_replace),'') }
    
    # ecriture dans le fichier
    write.table(c(go_term_direct,go_prot_direct$p.name[[1]]),ouptut_file_dir_prots, quote = FALSE, row.names = FALSE, col.names=FALSE, append = TRUE) 
  }
}



################# Proteines DIRECTES + IMPLICITES
ouptut_file_indir_prots = paste(name_sp,'.indirects.sets.txt',sep='')

write(paste("# format: sets \n# version: 1.0 \n# strain: ",name_sp,"\n# date : ",date,"\n# comment: Gene Ontology terms")
      , file = ouptut_file_indir_prots, append = FALSE, sep = "") # reecrit le fichier 


for (got in (1:length(list_go))) {
  go_term = list_go[got]
  
  # recupere les goterm : acc, type, name
  query_term_indirect = paste0("MATCH (g:GOTerm {acc : '", go_term, "'}) return g.acc, g.term_type, g.name ") # query neo4j
  go_term_indirect = query_term_indirect %>% call_neo4j(connex) # envoie la requete + sauvegarde le resultat
  
  if (length(go_term_indirect$g.acc[[1]]) != 0) {   # verifie que la ligne du goterm recupere n'est pas vide
    
    # recupere les proteines correspondantes au goterm
    query_prot_indirect = paste0("MATCH (g:GOTerm {acc : '", go_term, "'}) -[*]-> (p:Protein) return p.name") # query neo4j
    go_prot_indirect = query_prot_indirect %>% call_neo4j(connex) # envoie la requete + sauvegarde le resultat
    
    go_prot_indirect_uniq = unique(go_prot_indirect$p.name[[1]]) # evite la redondance des prot
    
    
    for (prot in (1:length(go_prot_indirect_uniq))){   # garde que le nom de la prot, enleve le nom de l'espece devant
      go_prot_indirect_uniq[prot] = str_replace(go_prot_indirect_uniq[prot],(to_replace),'') }
    
    
    # ecriture dans le fichier
    write.table(c(go_term_indirect,go_prot_indirect_uniq),ouptut_file_indir_prots, quote = FALSE, row.names = FALSE, col.names=FALSE, append = TRUE) 
  }
}

    




















